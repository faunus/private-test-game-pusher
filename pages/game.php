<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>el-titulo</title>
    <meta name="description" content="">

    <!--    To ensure proper rendering and touch zoomin  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

    <script src="http://js.pusher.com/2.1/pusher.min.js" type="text/javascript"></script>
    <script src="../phaser/build/phaser.min.js"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">
You are using an <strong>outdated</strong> browser.
Please <a href="http://browsehappy.com/">upgrade your browser</a> or
    <a href="http://www.google.com/chromeframe/?redirect=true">
    activate Google Chrome Frame
</a> to improve your experience.
</p>
<![endif]-->
<script>
window.onload = function()
{
    Pusher.log = function(message) {
        if (window.console && window.console.log) {
            window.console.log(message);
        }
    };

    var currentUserID = Math.floor(getRandomArbitrary(0, 900000));

    console.log("%c current user: " + currentUserID, 'background: #222; color: #bada55');

    var pusher = new Pusher('6ed793b683023296712b', {
    authEndpoint: "./pusher_auth.php",
            auth: {
        params: {
            userID: currentUserID
                }
    }
        });

        var channel = pusher.subscribe('private-fau');

        channel.bind('pusher:subscription_succeeded', function() {
            console.log("%c jamon conectado", 'background: #222; color: #bada55');
        });

        channel.bind('pusher:subscription_error', function($param0) {
            console.log("%c " + $param0, 'background: #222; color: #bada55');
        });

        channel.bind('client-moves', function(data) {
            console.log("%c data:", 'background: #222; color: #bada55');
            console.log(data);

            updatedPlayer2.x = data.x;
            updatedPlayer2.y = data.y;
        });

    /**
     * w
     * h
     * rendering context {Phaser.CANVAS, Phaser.WEBGL, or Phaser.AUTO}
     * dom id to attach the context || append to body
     * object {}
     *
     * @type {Game}
     */

    var updatedPlayer1 = {
        userID: currentUserID,
        x: 0,
        y: 0,
        isMoving: false

    };

    var updatedPlayer2 = {
        userID: '',
        x: 0,
        y: 0,
        isMoving: false
    };

    var game = new Phaser.Game(
        window.innerWidth,
        window.innerHeight,
        Phaser.CANVAS,
        '',
        { preload: preload, create: create, update: update, render: render });



    /**
     * This function is called first. It should contain code to handle the loading of assets needed by your game.
     * I.e. game.load.image(), etc. Note: while 'preload' is running the game doesn't call the update or
     * render functions, instead it calls 2 special functions (if they exist): loadUpdate and loadRender.
     * You don't have to put just loader specific code in the preload function. You could also do things
     * like set the stage background color or scale modes. However to keep it clean and logical I would
     * suggest you do limit it to just loading assets.
     *
     * Note that you don't need to tell Phaser to start the load, it happens automatically.
     *
     * loadUpdate: This is a special-case function that is only called while assets are
     * preloading (as the standard update function is not). You could use it to do something like update a progress bar.
     *
     * loadRender: Most likely not needed (especially not for WebGL games) but this is an optional special-case
     * function that is called after update and should contain render specific code.
     */

    function preload() {

    }

    /**
     * The create function is called automatically once the preload has finished. Equally if you don't actually
     * load any assets at all or don't have a preload function then create is the first function called by Phaser.
     * In here you can safely create sprites, particles and anything else you need that may use assets the preload
     * will now have loaded for you. Typically this function would contain the bulk of your set-up code, creating
     * game objects and the like.
     */

    var cursors;
    var config = {
        move_distance: 100,
        velocity: 2
    };

    var p1, localPlayer1, localPlayer2;
    var p2;
    var p3;
    var p4;

    var points_vector = [];
    var SIZE = 30;
    var a = 30;
    var b = 20;
    var r = 4 * SIZE / 5;
    var POINTS = 100;

    var d1 = 0;
    var d2 = 0;
    var d3 = 0;
    var d4 = 0;

    function create() {

        this.time.advancedTiming = true;

        var m = Math.min(a, b);
        r = 4 * m / 5;

        var startPoint = {
            "centerX": game.world.centerX,
            "centerY": game.world.centerY
        };

        for(index = 0; index < POINTS; index++)
        {
            t = 2 * Math.PI * index / POINTS;

            points_vector.push(
                new Phaser.Point(
                    a + r * Math.cos(t),
                    a + r * Math.sin(t)
                )
            );
        }

        localPlayer1 = new Phaser.Point(startPoint.centerX - 400, startPoint.centerY);

        updatedPlayer1.x = localPlayer1.x;
        updatedPlayer1.y = localPlayer1.y;

        localPlayer2 = new Phaser.Point(startPoint.centerX - 400, startPoint.centerY);

        updatedPlayer2.x = localPlayer2.x;
        updatedPlayer2.y = localPlayer2.y;

        cursors = game.input.keyboard.createCursorKeys();
    }

    /**
     * The update (and render) functions are called every frame. So on a desktop that'd be around 60 time per second.
     * In update this is where you'd do things like poll for input to move a player, check for object collision, etc.
     * It's the heart of your game really.
     *
     * render: The render function is called AFTER the WebGL/canvas render has taken place, so consider it the
     * place to apply post-render effects or extra debug overlays. For example when building a game I will
     * often put the game into CANVAS mode only and then use the render function to draw lots of debug info
     * over the top of my game.
     *
     * Once render has completed the frame is over and it returns to update again.
     *
     * Note that you cannot use any of the above function names in your game other than for the use they were
     * intended above. What I mean by that is you should consider them as being 'reserved' as game system only
     * functions.
     */

    function render () {

        for (index = 0; index < POINTS; index++)
        {
            game.context.fillStyle = 'rgb(255,255,0)';
            game.context.fillRect(points_vector[index].x, points_vector[index].y, 4, 4);
        }

        game.context.fillStyle = 'rgb(255,0,0)';
        game.context.fillRect(localPlayer1.x, localPlayer1.y, 4, 4);

        game.context.fillStyle = 'rgb(0,0,255)';
        game.context.fillRect(localPlayer2.x, localPlayer2.y, 4, 4);

    }

    var random_radio;
    var random_pi;

        function gameIsAtPusherFrame() {
            return ( game.time.frames + 1 ) % 10 === 0;
        }

    function updateUserStatus() {
//        userStatus.x = currentPlayerOne.x;
//        userStatus.y = currentPlayerOne.y;
        channel.trigger("client-moves", updatedPlayer1);
    }

    function updatePlayer1() {
        if (cursors.left.isDown && gameIsAtPusherFrame() && ! updatedPlayer1.isMoving) {

            updatedPlayer1.x -= config.move_distance;
            updatedPlayer1.isMoving = true;
            updateUserStatus();
        }
        else if (cursors.right.isDown && gameIsAtPusherFrame() && ! updatedPlayer1.isMoving) {

            updatedPlayer1.x += config.move_distance;
            updatedPlayer1.isMoving = true;
            updateUserStatus();
        }
        else if (cursors.up.isDown && gameIsAtPusherFrame() && ! updatedPlayer1.isMoving) {

            updatedPlayer1.y += config.move_distance;
            updatedPlayer1.isMoving = true;
            updateUserStatus();
        }
        else if (cursors.down.isDown && gameIsAtPusherFrame() && ! updatedPlayer1.isMoving) {

            updatedPlayer1.y -= config.move_distance;
            updatedPlayer1.isMoving = true;
            updateUserStatus();
        }

        updateMovementPlayerOne();
    }

    function updateEffectsPlayerOne() {
        if (d1 % 1 === 0) {
            random_radio = getRandomArbitrary(0, 600);
        }

        if (d1 % 10 === 0) {
            random_pi = getRandomArbitrary(0, Math.PI);
        }
    }

    function rotatePlayer() {
        localPlayer1
            .rotate(localPlayer1.x,
                localPlayer1.y,
                game.math.wrapAngle(d1),
                true,
                2);
    }

    function updateEffects() {
        for (index = 0; index < POINTS; index++) {
            points_vector[index]
                .rotate(localPlayer1.x,
                    localPlayer1.y,
                    game.math.wrapAngle(d1 + index * random_pi),
                    true,
                    random_radio);
        }
    }

    function PlayerEffects() {
        updateEffects();
//        rotatePlayer();
        d1 += 4;
    }

    function playerMovement() {
        if (updatedPlayer1.isMoving) {
            if (updatedPlayer1.x > localPlayer1.x) {
                localPlayer1.x += config.velocity;
            }
            else if (updatedPlayer1.x < localPlayer1.x) {
                localPlayer1.x -= config.velocity;
            }
            else if (updatedPlayer1.y < localPlayer1.y) {
                localPlayer1.y -= config.velocity;
            }
            else if (updatedPlayer1.y > localPlayer1.y) {
                localPlayer1.y += config.velocity;
            }
            else {
                updatedPlayer1.isMoving = false;
            }
        }
    }

    function updateMovementPlayerOne() {

        playerMovement();
        updateEffectsPlayerOne();
        PlayerEffects();
    }

    function updatePlayer2() {

//        console.log(Math.abs(user2Status.x))
        if (updatedPlayer2.x > localPlayer2.x) {
            localPlayer2.x += config.velocity;
        }
        else if (updatedPlayer2.x < localPlayer2.x) {
            localPlayer2.x -= config.velocity;
        }
        else if (updatedPlayer2.y < localPlayer2.y) {
            localPlayer2.y -= config.velocity;
        }
        else if (updatedPlayer2.y > localPlayer2.y) {
            localPlayer2.y += config.velocity;
        }

//        player2.x = user2Status.x;
//        player2.y = user2Status.y;
    }

    function update() {

        updatePlayer2();
        updatePlayer1();
    }

        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }

    }
</script>
</body>
</html>